import Vue from 'vue'
import Vuex from 'vuex'

let host = window.location.protocol + "//" + window.location.host + "/api";

Vue.use(Vuex)

export default new Vuex.Store({

    state: {

        //这里放全局参数
        userMessage: '',
        config: {
            mobile: false,
            showDrawer: false,
            debug: true
        },
        isLogin: false,
        host: host,
        img: 'http://sandbox2.ykdepot.com/tp5//uploads/MerchInfo/merchid4/20210816/8331b7628de1cb2687b6269c41a2ab78.jpg',
        themeColor: "themeIndex",
        isMobile: false
    },

    mutations: {
        //这里是set方法
        setMobile(state, payload) {
            state.isMobile = payload
        }
    },
})
