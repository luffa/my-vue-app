import Vue from 'vue';
import Router from 'vue-router';

import Frame from "../menu/Frame";
import indexMenuRouter from "./common/indexMenuRouter";
import demoRouter from "./common/demoRouter";
import byRouter from "./common/byRouter";
import interviewRouter from "./common/interviewRouter";
import InterviewIndex from "../pages/interview/index";

Vue.use(Router);

const routes = [
    {
        path: '/',
        name: 'index',
        component: ()=>import(/* webpackChunkName: "Login" */ '@/pages/index'),
        children:[
            ...indexMenuRouter,
            ...demoRouter
        ]
    },
    // {
    //     path: '/:by',
    //     component: Frame,
    //     name:'by',
    //     children:[
    //         ...byRouter
    //     ]
    // },
    {
        path: '/:interviews',
        // component: InterviewIndex,
        component: ()=>import(/* webpackChunkName: "interviewIndex" */ '@/pages/interview/index'),
        name:'interviews',
        children:[
            ...interviewRouter
        ]
    }

];

const router = new Router({
    mode: 'history',
    routes
});


export default router