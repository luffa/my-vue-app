
export default [
    {
        path:'index',
        name:'首页',
        component:()=>import(/* webpackChunkName: "Index" */ '@/pages/home/index')
    },
    {
        path:'demo',
        name:'一些demo',
        component:()=>import(/* webpackChunkName: "Index" */ '@/pages/demo/index')
    },
    {
        path:'square',
        name:'随心广场',
        component:()=>import(/* webpackChunkName: "Index" */ '@/pages/square/index')
    },
    {
        path:'resume',
        name:'个人简历',
        component:()=>import(/* webpackChunkName: "Index" */ '@/pages/resume/index')
    },
]