export default [
    {
        path:'demo/d01',
        name:'高德地图',
        component:()=>import(/* webpackChunkName: "Index" */ '@/pages/demo/D01/index')
    },
    {
        path:'demo/d02',
        name:'QRCode二维码',
        component:()=>import(/* webpackChunkName: "Index" */ '@/pages/demo/D02/index')
    },
]