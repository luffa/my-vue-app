import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui'; // 2.1引入结构
import 'element-ui/lib/theme-chalk/index.css'; // 2.2引入样式
import './assets/scss/_variable.scss';
import VueAMap  from 'vue-amap';
import axios from 'axios'
import dayjs from "dayjs"

import router from './router/index.js';
import store from "./store/index.js";
import common from "./components/index"
import http from './request/axios.js'

import animated from "animate.css"
Vue.use(animated);

Vue.use(ElementUI); // 3.安装

// import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
// Vue.use(Antd);

//高德地图
Vue.use(VueAMap);
VueAMap.initAMapApiLoader({
  //自己的key：81298fa343888077f3dff87115c347be
  key: '5dbec3b1de68581a7f3cd932111641b1',
  //引入解析经纬度的组件
  plugin: [
    'AMap.Autocomplete',//输入提示插件
    'AMap.PlaceSearch',//POI搜索插件
    // 'AMap.Scale',//右下角缩略图插件 比例尺
    // 'AMap.OverView',//地图鹰眼插件
    'AMap.ToolBar',//地图工具条
    // 'AMap.MapType',//类别切换控件，实现默认图层与卫星图、实施交通图层之间切换的控制
    // 'AMap.PolyEditor', //编辑 折线多，边形
    // 'AMap.CircleEditor',//圆形编辑器插件
    "AMap.Geolocation", //定位控件，用来获取和展示用户主机所在的经纬度位置
    'Geocoder',
    'AMap.Geocoder'
  ],
  //plugin: ['Geocoder','AMap.AutoComplete','AMap.PlaceSearch'],
  // v: '1.4.4',
  // uiVersion: '1.0'
});

Vue.config.productionTip = false
Vue.prototype.$store = store
Vue.prototype.$axios2 = axios;
Vue.prototype.$http = http;
Vue.prototype.$dayjs = dayjs;

new Vue({
  router,
  store,
  common,
  render: h => h(App),
}).$mount('#app')
