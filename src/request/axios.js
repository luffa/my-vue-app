import axios from "axios";
import qs from "qs";
import store from "@/store/";
import router from "@/router/index";
import { Message } from "element-ui";

//环境的切换

//请求超时时间
axios.defaults.timeout = 10000;
// 加载全局的loading

// 配置post请求头 application/x-www-form-urlencoded;charset=UTF-8 multipart/form-data
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
// 配置get请求头
axios.defaults.headers.get['Content-Type'] = 'application/json;charset=UTF-8';

//配置接口地址 http://sandbox2.ykdepot.com /tp5/public/index.php?s=api/index/index
axios.defaults.baseURL =  '/';
// //根据自己配置的反向代理去设置不同环境的baeUrl
// //创建axios实例，在这里可以设置请求的默认配置
// axios.create({
//     //根据自己配置的反向代理去设置不同环境的baeUrl
//     baseURL: process.env.NODE_ENV === 'production' ? '' : '/index.php'
// });

//请求拦截器
axios.interceptors.request.use(
    config =>{
        //对文件进行处理
        if(!config.isFile){
            config.data =  qs.stringify(config.data);
        }
        //获取token
        //let token = JSON.parse(localStorage.getItem('userMessage')).token || "";
        //引去请求头
        // config.headers = {
        //     // multipart/form-data  application/x-www-form-urlencoded
        //     'Content-Type':'application/x-www-form-urlencoded;charset=UTF-8',
        //     //'token':JSON.stringify(token)
        // };
        return config;
    },
    error => {
        return Promise.error(error);
    }
);

//响应拦截器
axios.interceptors.response.use(
    response =>{
        console.log(response)
        if ((response.data.status === 0 && response.status === 200) || (response.data.code === 1 && response.status === 200)) {
            return Promise.resolve(response);
        }else if(response.data.status === 2 && response.data.msg === 'token不存在，退出登录'){
            Message({
                message: "登录过期，请重新登录",
                type: "error"
            });
            // 清除token
            localStorage.removeItem('token');
            window.localStorage.setItem('isLogin',false);
            // 跳转登录页面，并将要浏览的页面fullPath传过去，登录成功后跳转需要访问的页面
            setTimeout(() => {
                router.replace({
                    path: '/login',
                    query: {
                        redirect: router.currentRoute.fullPath
                    }
                });
            }, 1000);
            return Promise.reject(response);
        } else if(response.data.error_code === 0){
            return Promise.resolve(response);
        }else {
            return Promise.reject(response);
        }
    },
    // 服务器状态码不是200的情况
    error => {
        console.log("error")
        console.log(error)
        if (error.response.status) {
            switch (error.response.status) {
                // 401: 未登录
                // 未登录则跳转登录页面，并携带当前页面的路径
                // 在登录成功后返回当前页面，这一步需要在登录页操作。
                case 401:
                    router.replace({
                        path: '/login',
                        query: {redirect: router.currentRoute.fullPath}
                    });
                    break;
                // 403 token过期
                // 登录过期对用户进行提示
                // 清除本地token和清空vuex中token对象
                // 跳转登录页面
                case 403:
                    Message({
                        message: '登录过期，请重新登录',
                        type: "error"
                    });
                    // 清除token
                    localStorage.removeItem('token');
                    store.commit('loginSuccess', null);
                    // 跳转登录页面，并将要浏览的页面fullPath传过去，登录成功后跳转需要访问的页面
                    setTimeout(() => {
                        router.replace({
                            path: '/login',
                            query: {
                                redirect: router.currentRoute.fullPath
                            }
                        });
                    }, 1000);
                    break;
                // 404请求不存在
                case 404:
                    Message({
                        message: '网络请求不存在',
                        type: "error"
                    });
                    break;
                // 其他错误，直接抛出错误提示
                default:
                    Message({
                        message: error.response.data.message,
                        type: "error"
                    });
            }
            return Promise.reject(error.response);
        }
    }
);

//返回一个Promise(发送post请求)
function post(url, params) {
    let path = router.currentRoute.fullPath;
    // if(path !== '/login'){
    //     //获取token
    //     let token = JSON.parse(localStorage.getItem('userMessage')).token || "";
    //     params.token = token;
    // }
    return new Promise((resolve, reject) => {
        axios.post(url, params)
            .then(response => {
                resolve(response);
            }, err => {
                reject(err);
            })
            .catch((error) => {
                reject(error)
            })
    })
}

// 返回一个Promise(发送get请求)
function get(url, param,request = {}) {
    return new Promise((resolve, reject) => {
        axios.get(url, {params: param},request)
            .then(response => {
                resolve(response)
            }, err => {
                reject(err)
            })
            .catch((error) => {
                reject(error)
            })
    })
}

//导出专用axios
function exportExcel(url,params,name) {
    let token = JSON.parse(localStorage.getItem('userMessage')).token || "";
    params.token = token;
    return new Promise((resolve, reject) => {
        axios.post(url, params,{
            responseType: 'blob'
        })
            .then(response => {
                resolve(response);
            }, err => {
                const link = document.createElement('a');
                let blob = new Blob([err.data], {type: 'application/vnd.ms-excel'});
                link.style.display = 'none';
                link.href = URL.createObjectURL(blob);//创建url对象
                link.download = err.headers['content-disposition'];
                //link.download = err.headers['content-disposition'].substr(20); //下载后文件名
                link.download = name;
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
                URL.revokeObjectURL(link.href);//销毁url对象
                //reject(err);
            })
            .catch((error) => {
                reject(error)
            })
    })
}

//上传图片接口
function uploadFile(url,params){
    return new Promise((resolve, reject) => {
        // axios.defaults.baseURL = '/tp5/public/index.php?s=api/index/index' + url;
        // axios.defaults.baseURL = '';
        axios.post(url, params,{
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            methods: 'post',
            isFile: true
        }).then(response => {
            resolve(response);
        }).catch((error) => {
            reject(error)
        })
    })
}

function jsonp({ url, params, callback }) {
    return new Promise((resolve, reject) => {
        // 创建一个临时的 script 标签用于发起请求
        const script = document.createElement('script');
        // 将回调函数临时绑定到 window 对象，回调函数执行完成后，移除 script 标签
        window[callback] = data => {
            resolve(data);
            document.body.removeChild(script);
        };
        // 构造 GET 请求参数，key=value&callback=callback
        const formatParams = { ...params, callback };
        const requestParams = Object.keys(formatParams)
            .reduce((acc, cur) => {
                return acc.concat([`${cur}=${formatParams[cur]}`]);
            }, [])
            .join('&');
        // 构造 GET 请求的 url 地址
        const src = `${url}?${requestParams}`;
        script.setAttribute('src', src);
        document.body.appendChild(script);
    });
}



export default {
    get,
    post,
    jsonp,
    axios,
    exportExcel,
    uploadFile
}


