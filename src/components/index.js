import Vue from 'vue'
import tabCom from "@/components/common/tabCom";
import mapCom from "@/components/common/mapCom";
import textJumpCom from "@/components/common/textJumpCom";

Vue.component("mapCom", mapCom);
Vue.component("textJumpCom", textJumpCom);