export default [
    {
        label: 'CSS',
        name: 'css',
        component: 'cssView',
    },
    {
        label: 'HTML',
        name: 'html',
        component: 'cssView',
    },
    {
        label: 'JavaScript',
        name: 'js',
        component: 'cssView',
    },
    {
        label: 'VUE',
        name: 'vue',
        component: 'cssView',
    }
]