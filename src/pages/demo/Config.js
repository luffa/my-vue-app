export default [
    {
        name: "高德地图",
        imgUrl: require('../../../src/assets/img/WechatIMG18.png'),
        path: "demo/d01",
        content: "主要使用vue-amap结合高德地图api实现"
    },
    {
        name: "QRCode二维码",
        imgUrl: require('../../../src/assets/img/WechatIMG19.png'),
        path: "demo/d02",
        content: "主要使用qrcodejs2实现"
    }
]