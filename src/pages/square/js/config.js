const carouselList = [
    {
        id: 0,
        title:"春光乍泄",
        url: require('../../../../src/assets/img/video/WechatIMG30.jpeg'),
        sub: "1997年张国荣、梁朝伟主演电影",
        content: "《春光乍泄》是由王家卫执导，张国荣 、梁朝伟、 张震主演的剧情片，于1997年5月30日在中国香港上映。该片讲述了一对前往南美游玩的同性恋人因迷路而留在布宜诺斯艾利斯，之后两人因为一系列矛盾而分手的故事。1997年，该片入围第五十届戛纳国际电影节正式竞赛单元，王家卫凭借该片获得第五十届戛纳国际电影节最佳导演奖。梁朝伟获得第17届香港电影金像奖最佳男主角。"
    },
    {
        id: 1,
        title:"苍兰诀",
        url: require('../../../../src/assets/img/video/WechatIMG35.jpeg'),
        sub: "2022年虞书欣、王鹤棣主演电视剧",
        content: "讲述息兰一族被灭族，神女万年后重生成天界低阶仙女小兰花，无意间复活了困于昊天塔的月尊。为获得自由，东方青苍要牺牲小兰花的神女之魂解开身上咒术封印，在此过程中，这个断情绝爱的东方青苍却爱上了温顺可爱小精怪。"
    },
    {
        id: 2,
        title:"密室大逃脱",
        url: require('../../../../src/assets/img/video/WechatIMG37.jpeg'),
        sub: "2019年芒果TV实景解密体验秀电视节目",
        content: "节目每期几位嘉宾被困于不同主题的密闭空间内，仅能依靠推理细节暗示，逐渐开启新的空间，并搭配解锁各种机关来逃生，直到全员逃脱为止。"
    },
];
const pictureList = [
    {
        url: require('../../../../src/assets/img/picture/WechatIMG22.jpeg'),
    },
    {
        url: require('../../../../src/assets/img/picture/WechatIMG25.jpeg'),
    },
    {
        url: require('../../../../src/assets/img/picture/WechatIMG26.jpeg'),
    },
    {
        url: require('../../../../src/assets/img/picture/WechatIMG31.jpeg'),
    },
    {
        url: require('../../../../src/assets/img/picture/WechatIMG32.jpeg'),
    },
    {
        url: require('../../../../src/assets/img/picture/WechatIMG24.jpeg'),
    },
];
const wordList = [
    // {
    //     name:"杨绛",
    //     content: "你的问题主要在于读书不多而想的太多。"
    // },
    // {
    //     name:"《古兰经》",
    //     content: "山不过来，我就过去。"
    // },
    {
        name:"李宗盛《给自己的歌》",
        content: "情爱里无智者。"
    },
    {
        name:"陆游《沈园》",
        content: "曾是惊鸿照影来。"
    },
    {
        name:"周杰伦《晴天》",
        content: "从前从前，有个人爱你很久。"
    },
    {
        name:"电影《天气预报员》",
        content: "成年人的生活里没有容易二字。"
    },
    {
        name:"电影《春光乍泄》",
        content: "原来寂寞的时候，所有人都一样。"
    },

];

export default {
    carouselList,
    pictureList,
    wordList
}