//&high_quality=1 切换清晰度

const video1 = {
    url: "//player.bilibili.com/player.html?aid=559128873&bvid=BV1oe4y1S7Nq&cid=864907895&page=1&high_quality=1",
    title: "😊",
    from: "游戏来源于《天涯明月刀手游》"
}
const video2 = {
    url: "//player.bilibili.com/player.html?aid=986643145&bvid=BV13t4y1F7K4&cid=864919523&page=1&high_quality=1",
    title: "🦋",
    from: "游戏来源于《天涯明月刀手游》"
}

export default {
    video1,
    video2
}