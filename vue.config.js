//用webpack的话 这个不会生效
module.exports={
    devServer: {
        port: 8080,
        proxy: {
            '/api': {
                target: 'http://v.juhe.cn',
                changeOrigin: true,
                "ws": true,
                pathRewrite: {
                    '^/api': ''
                }
            },
            '/posts': {
                target: 'https://jsonplaceholder.typicode.com/posts/',
                changeOrigin: true,
                // pathRewrite: {
                //     '^/api': ''
                // }
            }
        }
    },
    css: {
        loaderOptions: {
            sass: {
                prependData: `@import "@/assets/scss/_variable.scss";` //引入全局变量
            }
        }
    }

};

